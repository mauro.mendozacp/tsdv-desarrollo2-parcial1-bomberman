﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpManager : MonoBehaviour
{
    [Range(0, 100)]
    [SerializeField] public int chance;
    [SerializeField] public List<GameObject> powerUpPrefabs = new List<GameObject>();

    [HideInInspector]
    public List<PowerUp> powerUpList = new List<PowerUp>();

    // Start is called before the first frame update
    void Start()
    {
        PowerUp.OnPowerUpKilledAsStatic += DestroyPowerUp;
    }

    void OnDestroy()
    {
        PowerUp.OnPowerUpKilledAsStatic -= DestroyPowerUp;
    }

    void DestroyPowerUp(PowerUp p)
    {
        powerUpList.Remove(p);
    }
}
