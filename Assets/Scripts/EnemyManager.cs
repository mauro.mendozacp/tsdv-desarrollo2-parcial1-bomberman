﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] public int quantity;
    [SerializeField] public List<GameObject> enemyPrefabList = new List<GameObject>();

    public List<Enemy> enemyList = new List<Enemy>();

    void Start()
    {
        Enemy.OnEnemyKilledAsStatic += EnemyDied;
    }

    void OnDestroy()
    {
        Enemy.OnEnemyKilledAsStatic -= EnemyDied;
    }

    private void EnemyDied(Enemy e)
    {
        enemyList.Remove(e);

        FindObjectOfType<Player>().EnemyKilled++;
        FindObjectOfType<Player>().Score += e.points;

        if (enemyList.Count == 0)
            FindObjectOfType<Portal>().Open = true;
    }
}
