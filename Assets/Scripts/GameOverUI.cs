﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverUI : MonoBehaviour
{
    [SerializeField] TMPro.TMP_Text result;
    [SerializeField] TMPro.TMP_Text score;
    [SerializeField] TMPro.TMP_Text life;
    [SerializeField] TMPro.TMP_Text timer;
    [SerializeField] TMPro.TMP_Text bombsSimultate;
    [SerializeField] TMPro.TMP_Text enemiesKilled;
    [SerializeField] TMPro.TMP_Text explosionDistance;

    const int secondsForMinutes = 60;

    // Start is called before the first frame update
    void Start()
    {
        if (GameManager.Instance.WinGame)
            AudioManager.Instance.Play("win");
        else
            AudioManager.Instance.Play("lose");

        result.text = "YOU ";
        if (GameManager.Instance.WinGame)
            result.text += "WIN!";
        else
            result.text += "LOSE!";

        int minutes = (int)(GameManager.Instance.Timer / 60);
        int seconds = (int)(GameManager.Instance.Timer % 60);
        timer.text = "Time: " + minutes.ToString() + ":";
        if (seconds < 10)
            timer.text += "0";
        timer.text += seconds.ToString();

        score.text = "Score: " + GameManager.Instance.Score;
        life.text = "Life: " + GameManager.Instance.Life;
        bombsSimultate.text = "Bombs Simultate: " + GameManager.Instance.BombsSimultate;
        enemiesKilled.text = "Enemies Killed: " + GameManager.Instance.EnemiesKilled;
        explosionDistance.text = "Explosion Distance: " + (int)(GameManager.Instance.ExplosionDistance);
    }

    public void BackToMenu()
    {
        GameManager.Instance.ResetGame();
        SceneManager.LoadScene(GameManager.Instance.MainMenuScene);
    }
}
