﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxManager : MonoBehaviour
{
    [SerializeField] public int quantity;
    [SerializeField] public GameObject prefab;

    public List<Box> boxList = new List<Box>();

    // Start is called before the first frame update
    void Start()
    {
        Box.OnBoxKilledAsStatic += BoxDied;
    }

    void OnDestroy()
    {
        Box.OnBoxKilledAsStatic -= BoxDied;
    }

    private void BoxDied(Box b)
    {
        boxList.Remove(b);
    }
}
