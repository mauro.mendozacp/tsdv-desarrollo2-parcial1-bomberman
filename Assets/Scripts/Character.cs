﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] protected int life;
    [SerializeField] public float moveSpeed;
    [SerializeField] public float turnSpeed;
    [SerializeField] public float hitDelay;
    [SerializeField] public LayerMask layerBlock;
    [SerializeField] protected string hittedSoundName;
    protected Animator anim;

    public enum Direction
    {
        Forward,
        Right,
        Back,
        Left
    }
    public Direction Dir { get; set; }
    protected bool hitted;

    public virtual int Life
    {
        get { return life; }
        set
        {
            if (value <= 0)
            {
                life = 0;
                Died = true;
            }
            else
                life = value;
        }
    }

    public float Size { get; set; }
    public float RayDistance { get; set; }
    public virtual bool Hitted { get => hitted; set => hitted = value; }
    public bool Died { get; set; }

    public bool CheckMove(float pos, Vector3 direction)
    {
        float dis = pos % Size;
        return (dis <= 1 || dis >= Size - 1) && !Physics.Raycast(transform.position, direction, RayDistance, layerBlock);
    }

    public IEnumerator Rotate()
    {
        Quaternion toRot = Quaternion.LookRotation(GetDirection(), Vector3.up);

        while(transform.rotation != toRot)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, toRot, turnSpeed * Time.deltaTime);

            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }

    Vector3 GetDirection()
    {
        Vector3 rotDir = new Vector3();

        switch (Dir)
        {
            case Direction.Forward:
                rotDir = Vector3.forward;
                break;
            case Direction.Right:
                rotDir = Vector3.right;
                break;
            case Direction.Back:
                rotDir = Vector3.back;
                break;
            case Direction.Left:
                rotDir = Vector3.left;
                break;
            default:
                rotDir = Vector3.zero;
                break;
        }

        return rotDir;
    }

    public Vector3 GetPositionCenter()
    {
        Vector3 auxPos = transform.position;
        float posSize = Size / 2;

        auxPos.x = posSize * (int)(auxPos.x / posSize);
        if (transform.position.x <= auxPos.x - 1 || transform.position.x >= auxPos.x + 1)
            auxPos.x += posSize;

        auxPos.z = posSize * (int)(auxPos.z / posSize);
        if (transform.position.z <= auxPos.z - 1 || transform.position.z >= auxPos.z + 1)
            auxPos.z += posSize;

        return auxPos;
    }
}