﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    [HideInInspector]
    private bool open;
    GameplayManager gm;
    [SerializeField] GameObject particles;
    [SerializeField] LayerMask portalLayer;

    public bool Open 
    { 
        get => open;
        set
        {
            open = value;

            particles.SetActive(open);
        }
    }

    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GameplayManager").GetComponent<GameplayManager>();
        Open = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (Open)
        {
            if (GameplayManager.CheckLayerInMask(portalLayer, other.gameObject.layer))
            {
                gm.FinishGame(true);
            }
        }
    }
}
