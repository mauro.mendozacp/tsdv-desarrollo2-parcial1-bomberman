﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField] float explosionTimer;
    [SerializeField] float explosionDelay;
    [SerializeField] GameObject mesh;
    [SerializeField] GameObject explosionParticle;

    public float Distance { get; set; }
    float timer;
    bool died;

    public delegate void BombKilledAction(Bomb b);
    public static BombKilledAction OnBombKilledAsStatic;

    GameplayManager gm;
    float offset;

    public struct Explosion
    {
        public Vector3 direction;
        public float distance;

        public Explosion(Vector3 dir, float dist)
        {
            direction = dir;
            distance = dist;
        }
    }
    Explosion[] explosion = new Explosion[4];

    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GameplayManager").GetComponent<GameplayManager>();
        offset = gm.size / 2;

        timer = 0f;
        died = false;

        explosion[0] = new Explosion(transform.forward, Distance);
        explosion[1] = new Explosion(transform.right, Distance);
        explosion[2] = new Explosion(-transform.forward, Distance);
        explosion[3] = new Explosion(-transform.right, Distance);

        StartCoroutine(BombTimer());
    }

    // Update is called once per frame
    void Update()
    {
        if (!died)
        {
            timer += Time.deltaTime;

            if (timer >= explosionTimer)
            {
                DestroyBomb();
            }
        }
    }

    void ExplosionDistance(Vector3 direction, ref float dist)
    {
        string layerhitted;
        Collider[] colliders = Physics.OverlapSphere(transform.position, gm.size / 2);

        foreach (Collider c in colliders)
        {
            layerhitted = LayerMask.LayerToName(c.transform.gameObject.layer);

            if (layerhitted == "player")
            {
                c.transform.gameObject.GetComponent<Player>().PlayerHitted();
            }

            if (layerhitted == "enemy")
            {
                c.transform.gameObject.GetComponent<Enemy>().EnemyHitted();
            }
        }

        if (Physics.Raycast(transform.position, direction, dist))
        {
            List<RaycastHit> sortHits = new List<RaycastHit>();
            RaycastHit[] hits = Physics.RaycastAll(transform.position, direction, dist);

            foreach (RaycastHit h in hits)
            {
                sortHits.Add(h);
            }
            sortHits.Sort(CompareDistanceHits);

            foreach (RaycastHit h in sortHits)
            {
                layerhitted = LayerMask.LayerToName(h.transform.gameObject.layer);

                if (layerhitted == "player")
                {
                    h.transform.gameObject.GetComponent<Player>().PlayerHitted();
                }

                if (layerhitted == "enemy")
                {
                    h.transform.gameObject.GetComponent<Enemy>().EnemyHitted();
                }

                if (layerhitted == "bomb" || layerhitted == "block" || layerhitted == "box")
                {
                    float distance = (int)(Vector3.Distance(transform.position, h.transform.position));
                    dist = (gm.size * distance / gm.size) - offset;

                    if (layerhitted == "box")
                    {
                        h.transform.gameObject.GetComponent<Box>().DestroyBox();
                    }
                    if (layerhitted == "bomb")
                    {
                        h.transform.parent.gameObject.GetComponent<Bomb>().DestroyBomb();
                    }

                    break;
                }
            }
        }
    }

    public int CompareDistanceHits(RaycastHit h1, RaycastHit h2)
    {
        float distance1 = Vector3.Distance(transform.position, h1.transform.position);
        float distance2 = Vector3.Distance(transform.position, h2.transform.position);

        return distance1.CompareTo(distance2);
    }

    public void DestroyBomb()
    {
        if (!died)
        {
            died = true;

            for (int i = 0; i < explosion.Length; i++)
            {
                ExplosionDistance(explosion[i].direction, ref explosion[i].distance);
            }
            ShowExplosionDirection();

            StopAllCoroutines();

            if (OnBombKilledAsStatic != null)
                OnBombKilledAsStatic(this);

            AudioManager.Instance.Play("bomb");

            mesh.SetActive(false);
            Destroy(gameObject, explosionDelay);
        }
    }

    void ShowExplosionDirection()
    {
        int initDistance = (int)gm.size;
        Vector3 auxPos;

        GameObject e = Instantiate(explosionParticle, transform.position, Quaternion.identity);
        e.transform.parent = transform;
        Destroy(e, explosionDelay);

        for (int i = 0; i < explosion.Length; i++)
        {
            for (int j = initDistance; j <= explosion[i].distance; j += initDistance)
            {
                auxPos = transform.position + explosion[i].direction * j;

                e = Instantiate(explosionParticle, auxPos, Quaternion.identity);
                e.transform.parent = transform;
                Destroy(e, explosionDelay);
            }
        }
    }

    IEnumerator BombTimer()
    {
        int quantityLerp = 4;
        float timer = 0f;
        float duration = explosionTimer / quantityLerp;

        Vector3 scaleOrigin = transform.localScale;
        Vector3 scaleTransform = transform.localScale * 1.25f;

        for (int i = 0; i < quantityLerp / 2; i++)
        {
            while (timer <= duration)
            {
                timer += Time.deltaTime;
                transform.localScale = Vector3.Lerp(scaleOrigin, scaleTransform, timer / duration);
                yield return new WaitForEndOfFrame();
            }
            timer = 0f;
            while (timer <= duration)
            {
                timer += Time.deltaTime;
                transform.localScale = Vector3.Lerp(scaleTransform, scaleOrigin, timer / duration);
                yield return new WaitForEndOfFrame();
            }
            timer = 0f;
        }

        yield return null;
    }
}