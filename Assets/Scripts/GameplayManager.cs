﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayManager : MonoBehaviour
{
    [SerializeField] GameObject playerPrefab;
    [SerializeField] GameObject goBox;
    [SerializeField] GameObject goPowerUp;
    [SerializeField] GameObject goEnemy;
    [SerializeField] GameObject goHUD;
    [SerializeField] GameObject goPlayerDirRef;
    [SerializeField] GameObject portalPrefab;

    [SerializeField] public Transform startPlayer;
    [SerializeField] public Vector3 startPosition;
    [SerializeField] public float size;
    [SerializeField] public int width;
    [SerializeField] public int height;

    Player player;
    BoxManager boxManager;
    EnemyManager enemyManager;
    PowerUpManager powerUpManager;
    public static bool Paused { get; set; }

    public static float Timer { get; set; }

    void Awake()
    {
        GameObject p = Instantiate(playerPrefab, startPlayer.position, Quaternion.identity);
        p.name = "Player";
        player = p.GetComponent<Player>();
        player.reference = goPlayerDirRef;
        goHUD.GetComponent<HUD>().StartListeners();
    }

    // Start is called before the first frame update
    void Start()
    {
        Timer = 0;
        boxManager = goBox.GetComponent<BoxManager>();
        enemyManager = goEnemy.GetComponent<EnemyManager>();
        powerUpManager = goPowerUp.GetComponent<PowerUpManager>();

        for (int i = 0; i < boxManager.quantity; i++)
        {
            GameObject box = Instantiate(boxManager.prefab);
            Box b = box.GetComponent<Box>();

            box.name = "Box " + (i + 1);
            box.transform.position = GetRandomPosition();
            box.transform.parent = goBox.transform;

            boxManager.boxList.Add(b);

            if (i == 0)
            {
                GameObject portal = Instantiate(portalPrefab);
                portal.transform.position = box.transform.position - new Vector3(0f, size / 2, 0f);
                portal.transform.parent = GameObject.FindGameObjectWithTag("Stage").transform;
                portal.name = "Portal";
            }
            else
            {
                if (Random.Range(0, 100) < powerUpManager.chance)
                {
                    int powerUpIndex = Random.Range(0, powerUpManager.powerUpPrefabs.Count);
                    GameObject powerUp = Instantiate(powerUpManager.powerUpPrefabs[powerUpIndex]);
                    PowerUp p = powerUp.GetComponent<PowerUp>();

                    powerUp.transform.position = box.transform.position;
                    powerUp.transform.parent = goPowerUp.transform;
                    powerUp.name = "Power Up " + (i + 1);

                    powerUpManager.powerUpList.Add(p);
                }
            }
        }

        for (int i = 0; i < enemyManager.quantity; i++)
        {
            int enemyIndex = Random.Range(0, enemyManager.enemyPrefabList.Count);
            GameObject enemy = Instantiate(enemyManager.enemyPrefabList[enemyIndex]);
            Enemy e = enemy.GetComponent<Enemy>();

            enemy.name = "Enemy " + (i + 1);
            enemy.transform.position = GetRandomPosition();
            enemy.transform.parent = goEnemy.transform;

            enemyManager.enemyList.Add(e);
        }

        /*int boxIndex = Random.Range(0, boxManager.boxList.Count);
        GameObject portal = Instantiate(portalPrefab);
        portal.transform.position = boxManager.boxList[boxIndex].transform.position - new Vector3(0f, size / 2, 0f);
        portal.transform.parent = GameObject.FindGameObjectWithTag("Stage").transform;
        portal.name = "Portal";*/

        /*for (int i = 0; i < powerUpManager.quantity; i++)
        {
            int powerUpIndex = Random.Range(0, powerUpManager.powerUpPrefabs.Count);
            GameObject powerUp = Instantiate(powerUpManager.powerUpPrefabs[powerUpIndex]);
            PowerUp p = powerUp.GetComponent<PowerUp>();

            powerUp.transform.position = GetBoxRandomPosition(boxManager.boxList[boxIndex].transform.position);
            powerUp.transform.parent = goPowerUp.transform;
            powerUp.name = "Power Up " + (i + 1);

            powerUpManager.powerUpList.Add(p);
        }*/

        AudioManager.Instance.Play("gameplay_theme");
        Paused = false;
    }

    // Update is called once per frame
    void Update()
    {
        Timer += Time.deltaTime;

        if (player.Died)
        {
            FinishGame(false);
        }
    }

    Vector3 GetRandomPosition()
    {
        Vector3 auxPos = new Vector3();
        Vector3 playerPos = startPlayer.position;
        playerPos.y = size / 2;

        bool repeat = false;
        do
        {
            repeat = false;
            auxPos.x = startPosition.x + size * (Random.Range(0, width));
            auxPos.y = size / 2;

            int doubleSize = (int)(size * 2);
            if ((int)(auxPos.x % doubleSize) == 0)
                auxPos.z = startPosition.z + size * (Random.Range(0, height));
            else
                auxPos.z = startPosition.z + doubleSize * (Random.Range(0, height / 2));

            if (auxPos == playerPos)
                repeat = true;
            else
            {
                foreach (Box b in boxManager.boxList)
                {
                    if (b.transform.position == auxPos)
                    {
                        repeat = true;
                        break;
                    }
                }
                foreach (Enemy e in enemyManager.enemyList)
                {
                    if (e.transform.position == auxPos)
                    {
                        repeat = true;
                        break;
                    }
                }
            }

        } while (repeat);

        return auxPos;
    }

    Vector3 GetBoxRandomPosition(Vector3 portalPosition)
    {
        Vector3 auxPos = new Vector3();
        bool repeat = false;
        do
        {
            int boxIndex = Random.Range(0, boxManager.boxList.Count);
            auxPos = boxManager.boxList[boxIndex].transform.position;
            repeat = false;

            if (portalPosition == auxPos)
                repeat = true;
            else
            {
                foreach (PowerUp p in powerUpManager.powerUpList)
                {
                    if (p.transform.position == auxPos)
                    {
                        repeat = true;
                        break;
                    }
                }
            }

        } while (repeat);

        return auxPos;
    }

    public void FinishGame(bool winGame)
    {
        GameManager.Instance.SetInfoGame(player, Timer, winGame, true);

        AudioManager.Instance.Stop("gameplay_theme");

        SceneManager.LoadScene(GameManager.Instance.GameOverScene);
    }

    public static bool CheckLayerInMask(LayerMask mask, int layer)
    {
        return mask == (mask | (1 << layer));
    }
}