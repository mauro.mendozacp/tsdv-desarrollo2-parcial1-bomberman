﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    [SerializeField] LayerMask playerLayer;
    [SerializeField] PowerUpData powerUp;

    public enum PowerUpType
    {
        Life,
        Bomb,
        Explosion
    }
    [System.Serializable]
    public struct PowerUpData
    {
        public PowerUpType type;
        public int value;
    }

    public delegate void PowerUpKilledAction(PowerUp e);
    public static PowerUpKilledAction OnPowerUpKilledAsStatic;

    void OnTriggerEnter(Collider other)
    {
        if (GameplayManager.CheckLayerInMask(playerLayer, other.gameObject.layer))
        {
            other.GetComponent<Player>().GetPowerUp(powerUp);

            if (OnPowerUpKilledAsStatic != null)
                OnPowerUpKilledAsStatic(this);

            AudioManager.Instance.Play("powerup");

            Destroy(gameObject);
        }
    }
}
