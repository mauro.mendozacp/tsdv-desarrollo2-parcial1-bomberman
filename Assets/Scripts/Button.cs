﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    public void Hover()
    {
        AudioManager.Instance.Play("click_hover");
    }

    public void Click()
    {
        AudioManager.Instance.Play("click");
    }
}
