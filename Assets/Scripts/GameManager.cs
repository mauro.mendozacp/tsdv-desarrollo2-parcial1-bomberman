﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public int MainMenuScene { get; } = 0;

    public int GameplayScene { get; } = 1;

    public int GameOverScene { get; } = 2;

    public int Score { get; set; }
    public int Life { get; set; }
    public float Timer { get; set; }
    public int BombsSimultate { get; set; }
    public int EnemiesKilled { get; set; }
    public float ExplosionDistance { get; set; }
    public bool GameOver { get; set; }
    public bool WinGame { get; set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void SetInfoGame(Player player, float timer, bool winGame, bool gameOver)
    {
        Score = player.Score;
        Life = player.Life;
        BombsSimultate = player.BombsQuantity;
        EnemiesKilled = player.EnemyKilled;
        ExplosionDistance = player.ExplosionDistance;
        Timer = timer;
        WinGame = winGame;
        GameOver = gameOver;
    }

    public void ResetGame()
    {
        Score = 0;
        Life = 0;
        Timer = 0f;
        BombsSimultate = 0;
        EnemiesKilled = 0;
        ExplosionDistance = 0f;
        GameOver = false;
        WinGame = false;
    }
}
