﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUI : MonoBehaviour
{
    [SerializeField] GameObject menu;
    [SerializeField] GameObject controls;
    [SerializeField] GameObject credits;

    void Start()
    {
        AudioManager.Instance.Play("menu_theme");
    }

    public void PlayGame()
    {
        AudioManager.Instance.Stop("menu_theme");
        SceneManager.LoadScene(GameManager.Instance.GameplayScene);
    }

    public void ShowControls()
    {
        menu.SetActive(false);
        controls.SetActive(true);
    }

    public void ShowCredits()
    {
        menu.SetActive(false);
        credits.SetActive(true);
    }

    public void ShowMenu()
    {
        menu.SetActive(true);
        credits.SetActive(false);
        controls.SetActive(false);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
