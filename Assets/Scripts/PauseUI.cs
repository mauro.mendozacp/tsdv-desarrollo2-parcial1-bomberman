﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseUI : MonoBehaviour
{
    [SerializeField] GameObject pauseMenu;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (GameplayManager.Paused)
                Resume();
            else
                Pause();
        }
    }

    public void Pause()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        GameplayManager.Paused = true;
    }

    public void Resume()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        GameplayManager.Paused = false;
    }

    public void BackToMenu()
    {
        Time.timeScale = 1f;
        GameplayManager.Paused = false;
        AudioManager.Instance.Stop("gameplay_theme");

        SceneManager.LoadScene(GameManager.Instance.MainMenuScene);
    }
}