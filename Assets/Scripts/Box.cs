﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    [SerializeField] GameObject boxOriginal;
    [SerializeField] GameObject boxShatter;

    public delegate void BoxKilledAction(Box b);
    public static BoxKilledAction OnBoxKilledAsStatic;

    public void DestroyBox()
    {
        if (OnBoxKilledAsStatic != null)
            OnBoxKilledAsStatic(this);

        AudioManager.Instance.Play("box");

        StartCoroutine(DestroyTrigger());

        boxOriginal.SetActive(false);
        boxShatter.SetActive(true);

        Destroy(gameObject, 2f);
    }

    IEnumerator DestroyTrigger()
    {
        yield return new WaitForSeconds(0.1f);
        GetComponent<BoxCollider>().enabled = false;
    }
}
