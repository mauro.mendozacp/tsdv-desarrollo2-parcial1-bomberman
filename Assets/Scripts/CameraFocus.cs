﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFocus : MonoBehaviour
{
    [SerializeField] float distance;
    [SerializeField] float angle;
    [SerializeField] GameObject reference;

    Transform player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        float height = (Mathf.Tan(angle * Mathf.PI / 180)) * distance;
        transform.position = player.position + (player.up * height) - (distance * reference.transform.forward);
        transform.LookAt(player);
    }
}
