﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : Character
{
    [SerializeField] public int points;
    [SerializeField] float idleDelay;
    [SerializeField] LayerMask playerLayer;

    public delegate void EnemyKilledAction(Enemy e);
    public static EnemyKilledAction OnEnemyKilledAsStatic;

    public override bool Hitted
    {
        get { return hitted; }
        set
        {
            hitted = value;
            anim.SetBool("Hitted", value);
        }
    }

    public enum State
    {
        Idle,
        Walking,
        Hitted
    }
    State state;
    GameplayManager gm;
    NavMeshAgent agent;
    Vector3 origin;
    Vector3 target;
    float timer;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        gm = GameObject.FindGameObjectWithTag("GameplayManager").GetComponent<GameplayManager>();
        anim = GetComponent<Animator>();

        agent.speed = moveSpeed;
        agent.angularSpeed = turnSpeed;
        Size = gm.size * 2;
        RayDistance = gm.size;
        Hitted = false;
        Died = false;

        timer = 0f;

        origin = transform.position;
        target = transform.position;
    }

    void Update()
    {
        if (!Died)
        {
            if (!Hitted)
            {
                UpdateState();
            }
        }
    }

    void UpdateState()
    {
        switch (state)
        {
            case State.Idle:
                timer += Time.deltaTime;
                if (timer >= idleDelay)
                {
                    origin = target;
                    StopAllCoroutines();
                    StartCoroutine(MoveSpeed(false));
                    SetRandomTarget();
                    timer = 0f;
                }

                break;
            case State.Walking:
                agent.SetDestination(target);

                Vector3 distanceToTarget = transform.position - target;
                if (distanceToTarget.magnitude < 0.2f)
                {
                    StopAllCoroutines();
                    StartCoroutine(MoveSpeed(true));
                    state = State.Idle;
                }

                break;
            default:
                break;
        }
    }

    void SetRandomTarget()
    {
        Vector3 auxPos = GetPositionCenter();

        Direction auxDir = (Direction)Random.Range(0, 4);

        switch (auxDir)
        {
            case Direction.Forward:
                if (CheckMove(auxPos.x, Vector3.forward))
                {
                    auxPos.z += gm.size;
                }

                break;
            case Direction.Right:
                if (CheckMove(auxPos.z, Vector3.right))
                {
                    auxPos.x += gm.size;
                }

                break;
            case Direction.Back:
                if (CheckMove(auxPos.x, Vector3.back))
                {
                    auxPos.z -= gm.size;
                }

                break;
            case Direction.Left:
                if (CheckMove(auxPos.z, Vector3.left))
                {
                    auxPos.x -= gm.size;
                }

                break;
            default:
                break;
        }

        target = auxPos;

        if (transform.position != target)
            state = State.Walking;
    }

    public void SetCenter(ref float pos)
    {
        float distance = (int)(pos % Size);

        if (distance != 0)
        {
            if (distance <= 1)
            {
                pos = Size * ((int)pos / Size);
            }
            else if (distance >= Size - 1)
            {
                pos = Size * ((int)pos / Size) + Size;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (!Hitted)
        {
            if (LayerMask.GetMask(LayerMask.LayerToName(other.gameObject.layer)) == playerLayer.value)
            {
                if (!other.GetComponent<Player>().Hitted)
                {
                    other.GetComponent<Player>().PlayerHitted();
                }
            }
            if (other.gameObject.layer == gameObject.layer)
            {
                target = origin;
            }
        }
    }

    public void EnemyHitted()
    {
        if (!Hitted)
        {
            Hitted = true;
            Life--;

            if (OnEnemyKilledAsStatic != null)
                OnEnemyKilledAsStatic(this);

            AudioManager.Instance.Play(hittedSoundName);

            Destroy(gameObject, hitDelay);
        }
    }

    IEnumerator MoveSpeed(bool reverse)
    {
        float duration = 1f;

        while (timer <= duration)
        {
            timer += Time.deltaTime;

            if (!reverse)
                anim.SetFloat("Speed", timer);
            else
                anim.SetFloat("Speed", duration - timer);

            yield return new WaitForEndOfFrame();
        }
    }
}
