﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : Character
{
    [SerializeField] string recoverySoundName;
    [SerializeField] private int bombsQuantity;
    [SerializeField] private float explosionDistance;
    [SerializeField] public float invulnerableDelay;
    [SerializeField] GameObject bombPrefab;
    [SerializeField] GameObject mesh;

    public override int Life
    {
        get { return life; }
        set
        {
            if (value <= 0)
            {
                life = 0;
                Died = true;
            }
            else
                life = value;
            RecieveLifeEvent?.Invoke();
        }
    }
    public override bool Hitted
    {
        get { return hitted; }
        set
        {
            hitted = value;
            anim.SetBool("Hitted", value);
            
            if(hitted)
                AudioManager.Instance.Play(hittedSoundName);
        }
    }

    private int score;
    public int Score
    {
        get => score;
        set
        {
            score = value;
            RecieveScoreEvent?.Invoke();
        }
    }
    public int BombsQuantity
    {
        get => bombsQuantity;
        set
        {
            bombsQuantity = value;
            RecieveBombsEvent?.Invoke();
        }
    }
    public float ExplosionDistance
    {
        get => explosionDistance;
        set 
        { 
            explosionDistance = value;
            RecieveExplosionEvent?.Invoke();
        }
    }
    public int EnemyKilled
    {
        get => enemyKilled;
        set
        { 
            enemyKilled = value;
            RecieveEnemyKilledEvent?.Invoke();
        }
    }
    public bool Invulnerable { get; set; }
    bool recovery;
    public bool Recovery
    {
        get { return recovery; }
        set
        {
            recovery = value;
            anim.SetBool("Recovery", value);

            if (recovery && !Died)
                AudioManager.Instance.Play(recoverySoundName);
        }
    }
    public Vector3 StartPosition { get; set; }
    public List<Bomb> bombList = new List<Bomb>();

    GameplayManager gm;
    GameObject spawnBombs;
    public GameObject reference;
    private int enemyKilled;
    IEnumerator eRotate;

    public UnityEvent RecieveLifeEvent { get; set; } = new UnityEvent();
    public UnityEvent RecieveScoreEvent { get; set; } = new UnityEvent();
    public UnityEvent RecieveBombsEvent { get; set; } = new UnityEvent();
    public UnityEvent RecieveExplosionEvent { get; set; } = new UnityEvent();
    public UnityEvent RecieveEnemyKilledEvent { get; set; } = new UnityEvent();

    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GameplayManager").GetComponent<GameplayManager>();
        anim = GetComponent<Animator>();

        Life = life;
        Dir = Direction.Forward;
        Size = gm.size * 2;
        RayDistance = gm.size / 2;
        Hitted = false;
        Died = false;
        Score = 0;
        BombsQuantity = bombsQuantity;
        ExplosionDistance = explosionDistance;
        EnemyKilled = 0;
        Invulnerable = false;
        StartPosition = gm.startPlayer.position;

        Bomb.OnBombKilledAsStatic += BombDied;
        spawnBombs = GameObject.FindGameObjectWithTag("SpawnBombs");
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameplayManager.Paused)
        {
            if (!Died)
            {
                if (!Hitted && !Recovery)
                {
                    SpawnBomb();
                    Move();
                }
                else
                    anim.SetFloat("Speed", 0f);
            }
        }
    }

    void Move()
    {
        if (Input.anyKey)
        {
            float animSpeed = anim.GetFloat("Speed");
            animSpeed += (Mathf.Abs(Input.GetAxis("Horizontal")) > Mathf.Epsilon) ? Mathf.Abs(Input.GetAxis("Horizontal")) : Mathf.Abs(Input.GetAxis("Vertical"));
            anim.SetFloat("Speed", animSpeed);

            Vector3 auxPos = transform.position;
            Direction auxDir = Dir;

            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                auxDir = Direction.Right;
                if (CheckMove(auxPos.z, reference.transform.right))
                {
                    auxPos.x += moveSpeed * Time.deltaTime;
                    SetCenter(ref auxPos.z);
                }
            }
            else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                auxDir = Direction.Left;
                if (CheckMove(auxPos.z, -reference.transform.right))
                {
                    auxPos.x -= moveSpeed * Time.deltaTime;
                    SetCenter(ref auxPos.z);
                }
            }
            else if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                auxDir = Direction.Forward;
                if (CheckMove(auxPos.x, reference.transform.forward))
                {
                    auxPos.z += moveSpeed * Time.deltaTime;
                    SetCenter(ref auxPos.x);
                }
            }
            else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            {
                auxDir = Direction.Back;
                if (CheckMove(auxPos.x, -reference.transform.forward))
                {
                    auxPos.z -= moveSpeed * Time.deltaTime;
                    SetCenter(ref auxPos.x);
                }
            }

            transform.position = auxPos;

            if (Dir != auxDir)
            {
                if (eRotate != null)
                    StopCoroutine(eRotate);
                Dir = auxDir;
                eRotate = Rotate();
                StartCoroutine(eRotate);
            }
        }
        else
            anim.SetFloat("Speed", 0f);
    }

    void SpawnBomb()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            if (BombsQuantity > 0)
            {
                Vector3 bombPos = GetPositionCenter();
                bombPos.y += 1f;

                if (CheckPositionBomb(bombPos))
                {
                    GameObject bomb = Instantiate(bombPrefab);
                    Bomb b = bomb.GetComponent<Bomb>();
                    b.Distance = ExplosionDistance * 2;

                    bomb.name = "Bomb " + (bombList.Count + 1);
                    bomb.transform.position = bombPos;
                    bomb.transform.parent = spawnBombs.transform;

                    bombList.Add(b);
                    BombsQuantity--;
                    RecieveBombsEvent?.Invoke();
                }
            }
        }
    }

    public void SetCenter(ref float pos)
    {
        float distance = pos % Size;

        if (distance != 0)
        {
            if (distance <= 1)
            {
                float center = Size * ((int)pos / Size);
                pos -= moveSpeed * Time.deltaTime;

                if (pos < center)
                    pos = center;
            }
            else if (distance >= Size - 1)
            {
                float center = Size * ((int)pos / Size) + Size;
                pos += moveSpeed * Time.deltaTime;

                if (pos > center)
                    pos = center;
            }
        }
    }

    public bool CheckPositionBomb(Vector3 bombPos)
    {
        foreach (Bomb bomb in bombList)
        {
            if (bomb.transform.position == bombPos)
            {
                return false;
            }
        }

        return true;
    }

    public void GetPowerUp(PowerUp.PowerUpData p)
    {
        switch (p.type)
        {
            case PowerUp.PowerUpType.Life:
                Life += p.value;
                break;
            case PowerUp.PowerUpType.Bomb:
                BombsQuantity += p.value;
                break;
            case PowerUp.PowerUpType.Explosion:
                ExplosionDistance += p.value;
                break;
            default:
                break;
        }
    }

    void BombDied(Bomb b)
    {
        bombList.Remove(b);
        BombsQuantity++;
        RecieveBombsEvent?.Invoke();
    }

    public void PlayerHitted()
    {
        if (!Hitted && !Invulnerable)
        {
            Hitted = true;
            StartCoroutine(PlayerDead());
        }
    }

    public IEnumerator PlayerDead()
    {
        yield return new WaitForSeconds(hitDelay);

        Life--;
        Invulnerable = true;
        Recovery = true;
        Hitted = false;
        transform.position = StartPosition;

        float invulTimerInFive = 0.5f;
        Color colorOrigin = mesh.GetComponent<Renderer>().material.color;
        Color colorTransform = new Color(0.4f, 0.4f, 0.4f, 1f);

        for (int i = 0; i < invulnerableDelay; i++)
        {
            mesh.GetComponent<Renderer>().material.color = colorTransform;
            yield return new WaitForSeconds(invulTimerInFive);

            mesh.GetComponent<Renderer>().material.color = colorOrigin;
            if (i + 1 < 2)
                yield return new WaitForSeconds(invulTimerInFive);

            if (i <= invulnerableDelay / 2)
                Recovery = false;
        }
        Invulnerable = false;

        yield return null;
    }

    void OnDestroy()
    {
        Bomb.OnBombKilledAsStatic -= BombDied;
    }
}