﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    [SerializeField] TMPro.TMP_Text score;
    [SerializeField] TMPro.TMP_Text life;
    [SerializeField] TMPro.TMP_Text timer;
    [SerializeField] TMPro.TMP_Text bombSimultate;
    [SerializeField] TMPro.TMP_Text enemiesKilled;
    [SerializeField] TMPro.TMP_Text explosionDistance;

    Player player;

    void Start()
    {
        SetTimer();   
    }

    void Update()
    {
        SetTimer();
    }

    void OnDestroy()
    {
        player.RecieveLifeEvent.RemoveListener(SetLife);
        player.RecieveScoreEvent.RemoveListener(SetScore);
        player.RecieveBombsEvent.RemoveListener(SetBombs);
        player.RecieveExplosionEvent.RemoveListener(SetExplosion);
        player.RecieveEnemyKilledEvent.RemoveListener(SetEnemiesKilled);
    }

    public void StartListeners()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        player.RecieveLifeEvent.AddListener(SetLife);
        player.RecieveScoreEvent.AddListener(SetScore);
        player.RecieveBombsEvent.AddListener(SetBombs);
        player.RecieveExplosionEvent.AddListener(SetExplosion);
        player.RecieveEnemyKilledEvent.AddListener(SetEnemiesKilled);
    }

    void SetTimer()
    {
        int minutes = (int)(GameplayManager.Timer / 60);
        int seconds = (int)(GameplayManager.Timer % 60);

        timer.text = minutes + ":";
        if (seconds < 10)
            timer.text += "0";
        timer.text += seconds;
    }

    void SetLife()
    {
        life.text = player.Life.ToString();
    }

    void SetScore()
    {
        score.text = "Score: " + player.Score.ToString();
    }

    void SetBombs()
    {
        bombSimultate.text = player.BombsQuantity.ToString();
    }

    void SetExplosion()
    {
        explosionDistance.text = player.ExplosionDistance.ToString();
    }

    void SetEnemiesKilled()
    {
        enemiesKilled.text = player.EnemyKilled.ToString();
    }
}
